#!/usr/bin/python3
#
#
import sys
from time import gmtime, strftime
import random
import config
import slixmpp

# Range of temerature for trigger xmpp_send
TEMP_ALARM_MAX = 80
TEMP_ALARM_MIN = 70

# Messages for xmpp_send
LIST_MOOD = []
LIST_MOOD.append("Ofen Temp: ")
LIST_MOOD.append("Heb Deinen Arsch und leg Holz nach! ")
LIST_MOOD.append("Na, mein Lieber, willst Du mich nicht mal besuchen? ")
LIST_MOOD.append("Sei so nett und leg Holz nach! ")
LIST_MOOD.append("Es wird kalt! ")
LIST_MOOD.append("Im Keller gibt's was zu tun! ")
LIST_MOOD.append("Ich friere! ")
LIST_MOOD.append("Für wohlige Wärme sorgt Holz im Ofen! ")


class SendMsgBot(slixmpp.ClientXMPP):

    """
    A basic Slixmpp bot that will log in, send a message,
    and then log out.
    """

    def __init__(self, jid, password, recipient, message):
        slixmpp.ClientXMPP.__init__(self, jid, password)

        # The message we wish to send, and the JID that
        # will receive it.
        self.recipient = recipient
        self.msg = message

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)

    async def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        self.send_presence()
        await self.get_roster()

        self.send_message(mto=self.recipient,
                          mbody=self.msg,
                          mtype='chat')

        self.disconnect()


def read_last_temp_from_file(filename):
    """read last registered temp from file"""
    try:
        with open(filename) as f:
            lines = f.readlines()
    except IOError:
        log_message = ("Error: can\'t find file or read data" + filename)
        print(log_message)

    lines = [x.strip() for x in lines]
    return lines[9]


def send_xmpp(xmpp_message):
    """ send_xmpp"""
    print("send_xmpp...")
    xmpp = SendMsgBot(
        config.xmpp_jid,
        config.xmpp_password,
        config.xmpp_jid_target,
        xmpp_message)
    xmpp.register_plugin('xep_0030')  # Service Discovery
    xmpp.register_plugin('xep_0199')  # XMPP Ping

    # Connect to the XMPP server and start processing XMPP stanzas.
    xmpp.connect()
    xmpp.process(forever=False)


def lets_rock():
    """lets rock """
    # read and temperatures
    filename = "/home/pi/home-tech-local/public_html/temp_2_last.html"
    temp_last = read_last_temp_from_file(filename)
    print("temp from file last." + temp_last)
    filename = "/home/pi/home-tech-local/public_html/temp_2.html"
    temp = read_last_temp_from_file(filename)
    print("temp from file......" + temp)

    # compare temperatures
    if temp < temp_last:
        if int(temp) < TEMP_ALARM_MAX and int(temp) > TEMP_ALARM_MIN:
            print(temp)
            send_xmpp(random.choice(LIST_MOOD) + temp + " °C")


if __name__ == '__main__':
    print("\nLet's go")
    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    lets_rock()
    # send_xmpp("test")
    print("Let's go home")
