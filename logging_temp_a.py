#!/usr/bin/python
# -*- coding: utf-8 -*-
# Thanx to:
# https://github.com/GolemMediaGmbH/OfficeTemperature/blob/master/Raspberry_Pi/Raspberry.py

import argparse
import re
from time import gmtime, strftime
import requests
import RPi.GPIO as GPIO
import config


def read_temperature(conf_sensor_pfad):
    """read temperature from sensor"""
    temperature = None
    try:
        datei = open(conf_sensor_pfad, "r")
        zeile = datei.readline()
        if re.match(r"([0-9a-f]{2} ){9}: crc=[0-9a-f]{2} YES", zeile):
            zeile = datei.readline()
            m = re.match(r"([0-9a-f]{2} ){9}t=([+-]?[0-9]+)", zeile)
            if m:
                temperature = m.group(2)
                datei.close()
                print "temperature from sensor..." + temperature
    except IOError:
        print "Error reading Sensor"
    return temperature


def send_temp(conf_sensor_nr, temp):
    """register current temp in database"""
    payload = {'action': 'add_temp', 'pa': conf_sensor_nr, 'pb': temp}
    try:
        res = requests.get(
            config.logging_url, params=payload,
            auth=(config.logging_user, config.logging_pw))
        print "return message from sent_temp:"
        print res
    except requests.exceptions.RequestException as e:
        print e


def read_temperature_from_file(filename):
    """read last registered temperature from file"""
    try:
        with open(filename) as f:
            lines = f.readlines()
    except IOError as (errno, strerror):
        log_message = ("write_from_file: I/O error({0}): {1}".format(
            errno, strerror) + ": " + filename)
        print log_message
        return None

    lines = [x.strip() for x in lines]
    # print lines
    # pos = lines.index("<body>")
    # print pos
    return lines[8]


def write_temperature_in_file(conf_sensor_nr, temperature, filename):
    """write html file with temperature for fhem"""
    try:
        f_html_temperature = open(filename, 'w')
    except IOError as (errno, strerror):
        log_message = ("write_to_file: I/O error({0}): {1}"
                       .format(errno, strerror) + ": " + filename)
        print log_message
    else:
        f_html_temperature.write("<!DOCTYPE html>\n")
        f_html_temperature.write('<html lang="en">\n')
        f_html_temperature.write('<head>\n')
        f_html_temperature.write('<meta charset="utf-8">\n')
        f_html_temperature.write(
            '<meta name="viewport" content="width=device-width, '
            + 'initial-scale=1">\n')
        f_html_temperature.write('<title>Home-Tech local</title>\n')
        f_html_temperature.write('</head>\n')
        f_html_temperature.write('<body>\n')
        f_html_temperature.write(temperature)
        f_html_temperature.write('\n</body>\n')
        f_html_temperature.write('</html>\n')
        f_html_temperature.close
        print "html file written............" + temperature


def main_stack(conf_sensor_nr, conf_sensor_id):
    """calling main functions"""
    #temperature = read_temperature(config.sensor_temp_01_id)
    temperature = read_temperature(conf_sensor_id)

    if temperature is None:
        print "temperature from sensor....not available"
        return

    temperature_in_2_digits = int(temperature) / 1000
    print "temperature current.........." + str(temperature_in_2_digits) + "°C"

    # read from local html file
    temperature_old_current = read_temperature_from_file(
        config.path_file_html_temperature_ta_current
        + "_" + str(conf_sensor_nr) + ".html")

    if temperature_old_current is None:
        # write current temperature in all files
        print "temperature from file...not available"
    else:
        print "temperature from file........" + temperature_old_current

    if temperature_in_2_digits != temperature_old_current:
        write_temperature_in_file(
            conf_sensor_nr, str(temperature_in_2_digits),
            config.path_file_html_temperature_ta_current
            + "_" + str(conf_sensor_nr) + ".html")

    # send to db
    if config.logging_to_web == "Y":
        send_temp(conf_sensor_nr, temperature)
    else:
        print "not logging to web-db"


if __name__ == '__main__':
    print "Let's go"
    print strftime("%Y-%m-%d %H:%M:%S")
    parser = argparse.ArgumentParser()
    parser.add_argument('sensor_nr', metavar='N', type=int,
                        help='Sensor-Nr. for the logger')
    args = parser.parse_args()
    if args.sensor_nr:
        print args.sensor_nr
        conf_sensor_nr = str(args.sensor_nr)
        sensor_ids = [config.sensor_temp_01_id,
                      config.sensor_temp_02_id,
                      config.sensor_temp_03_id,
                      config.sensor_temp_04_id,
                      config.sensor_temp_05_id]
        conf_sensor_id = sensor_ids[args.sensor_nr - 1]

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    main_stack(conf_sensor_nr, conf_sensor_id)
    print "Let's g home"
    print ""
